import logging

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import pandas as pd

from lib.manalysis import SchedulabilityStatus
from lib.mplatform import Platform


class SystemState:
    status: SchedulabilityStatus = SchedulabilityStatus.NOT_SCHEDULABLE
    next_states: list
    computer_failed_names: list
    platform: Platform
    name: str
    graph_name: str

    def __init__(self, platform: Platform, name: str):
        self.platform = platform
        self.next_states = []
        self.computer_failed_names = []
        self.name = name

    def set_status(self, status: SchedulabilityStatus):
        self.status = status

    def add_edge(self, state, failed_computer_name):
        self.next_states.append(state)
        self.computer_failed_names.append(failed_computer_name)

    def __str__(self):
        ptf_csv = str(self.platform)
        export_str = f"State Name,Schedulability Status,Balance Ratio\n"
        export_str += f"{self.name},{self.status},{self.platform.balance_ratio()}\n"
        export_str += (
            f"Next States:,{str([state.name for state in self.next_states])}\n"
        )
        export_str += ptf_csv

        return export_str


class SystemReconfigurationGraph:
    def __init__(self, initial_state: SystemState):
        self._root = initial_state
        self.from_state = []
        self.to_state = []
        self.states_name = []
        self.states_name_graph = []
        self.states_characteristic = []
        self.edge_names = []
        self.counter = 0

        self.fail_state = SystemState(self._root.platform, "F")

        self.set_graph_nodes_name(self._root)

        self.fill_matrix(self._root)

        self.set_graph_node_name(self.fail_state, "F")

        # self.from_state[0] = self._root.name
        # index = 0
        # for state in self._root.next_states:
        #     self.to_state[index] = state.name
        #     index+=1

        logging.info(self.from_state)
        logging.info(self.to_state)

    def set_graph_node_name(self, state: SystemState, name=""):
        if state.name not in self.states_name:
            if name == "":
                name = f"S{self.counter}"
            self.states_name.append(state.name)
            self.states_name_graph.append(name)
            state.graph_name = name
            self.counter += 1
            if state.status == SchedulabilityStatus.SCHEDULABLE:
                self.states_characteristic.append("green")
            elif state.status == SchedulabilityStatus.SCHEDULABLE_DEGRADED:
                self.states_characteristic.append("orange")
            else:
                self.states_characteristic.append("red")
            # self.states_characteristic.append(state.status.value * 50 + 50)

    def set_graph_nodes_name(self, state: SystemState):
        self.set_graph_node_name(state)

        for next_state in state.next_states:
            self.set_graph_nodes_name(next_state)

    def find_transition(self, from_name, to_name):
        for i, value in enumerate(self.from_state):
            if value == from_name and self.to_state[i] == to_name:
                return i
        return -1

    # def is_transition_already_added(self):
    #     for i, value in enumerate(self.from_state):
    #         to_name = self.to_state[i]

    def fill_matrix(self, state: SystemState):
        # if state.name not in self.states_name:
        #     self.states_name.append(state.name)
        #     self.states_name_graph.append(f"S{self.counter}")
        #     state.graph_name = f"S{self.counter}"
        #     self.counter += 1
        #     if state.status == SchedulabilityStatus.SCHEDULABLE:
        #         self.states_characteristic.append("green")
        #     elif state.status == SchedulabilityStatus.SCHEDULABLE_DEGRADED:
        #         self.states_characteristic.append("orange")
        #     else:
        #         self.states_characteristic.append("red")
        # self.states_characteristic.append(state.status.value * 50 + 50)

        for i, next_state in enumerate(state.next_states):
            if next_state.status == SchedulabilityStatus.NOT_SCHEDULABLE:
                to_name = "F"
            else:
                to_name = next_state.graph_name

            index = self.find_transition(from_name=state.graph_name, to_name=to_name)
            if index == -1:
                self.from_state.append(state.graph_name)
                # self.to_state.append(to_name)
                self.to_state.append(next_state.graph_name)
                self.edge_names.append(state.computer_failed_names[i])
            else:
                self.edge_names[index] += f",{state.computer_failed_names[i]}"

        for next_state in state.next_states:
            self.fill_matrix(next_state)

    def graph(self, save_file_path=""):

        labels = {}
        for i in range(len(self.from_state)):
            # edges.append([self.from_state[i], self.to_state[i]])
            key = (self.from_state[i], self.to_state[i])

            labels[key] = f"{self.edge_names[i]}"
        # This time a pair can appear 2 times, in one side or in the other!
        df = pd.DataFrame({"from": self.from_state, "to": self.to_state})

        # And a data frame with characteristics for your nodes
        carac = pd.DataFrame(
            {"ID": self.states_name_graph, "myvalue": self.states_characteristic}
        )

        # Build your graph. Note that we use the DiGraph function to create the graph!
        G = nx.from_pandas_edgelist(df, "from", "to", create_using=nx.DiGraph())

        # The order of the node for networkX is the following order:
        G.nodes()
        # NodeView(('A', 'D', 'B', 'C', 'E'))

        # Thus, we cannot give directly the 'myvalue' column to netowrkX, we need to arrange the order!
        print(labels)
        # self.states_characteristic = [0, 25, 50, 75, 100, 125, 150, 175, 200, 225]
        print({"ID": self.states_name, "myvalue": self.states_characteristic})
        # Here is the tricky part: I need to reorder carac, to assign the good color to each node
        carac = carac.set_index("ID")
        carac = carac.reindex(G.nodes())
        pos = nx.nx_pydot.graphviz_layout(G, prog="dot")
        # Make the graph
        # nx.draw(G, with_labels=True, node_size=1500, alpha=0.3, arrows=True)
        nx.draw(
            G,
            pos=nx.nx_pydot.graphviz_layout(G, prog="dot"),
            with_labels=True,
            node_color=carac["myvalue"],
        )
        nx.draw_networkx_edge_labels(G, pos, edge_labels=labels, font_color="red")
        plt.title("Directed")
        plt.axis("off")
        if save_file_path == "":
            plt.show()
        else:
            plt.savefig(save_file_path)
