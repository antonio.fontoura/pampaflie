class PartitionEmpty(Exception):
    pass


class PartitionFullError(Exception):
    pass


class ComputerNotSuitableError(Exception):
    pass


class PartitionCritical(Exception):
    pass


class ApplicationDependendConstraint(Exception):
    pass


class AllocationError(Exception):
    pass
